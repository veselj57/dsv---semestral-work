package distributed_chat.ui

import distributed_chat.core.*
import distributed_chat.core.Command
import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority
import javafx.scene.text.FontWeight

import tornadofx.*
import javafx.stage.Stage
import javafx.scene.control.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import java.net.InetAddress


class ChatScreen : View("Person Editor") {
    override val root = BorderPane()
    val model: LoginViewModel by inject()


    var chats = hashMapOf<NodeID, Chat>()
    val list = mutableListOf<NodeID>().observable()

    val id = NodeID(InetAddress.getLocalHost().hostAddress, model.yourPort)


    val enable = SimpleBooleanProperty(false)
    val uiEnable = SimpleBooleanProperty(true)
    var selected: NodeID? = null
    var titleField : Label by singleAssign()
    var chatField : TextArea by singleAssign()
    var messageField : TextField by singleAssign()

    val uiChannel = Channel<UIUpdate>()

    val node = ClusterNode(id, uiChannel)


    init {
       GlobalScope.launch(Dispatchers.IO) {
           for( update in uiChannel){
               node.logger.info { "Updating UI" }
               runLater {
                   uiEnable.set(update.uiEnable)

                   chats = update.chats

                   list.clear()
                   list.addAll(update.chats.keys)

                   if (!list.contains(selected)) {
                       selected = null
                   }
                   renderChat()
               }
           }
       }

        title = id.toString()
        if (!model.nodeIPP.isEmpty()) {
            GlobalScope.launch (Dispatchers.IO) {
                node.connect(NodeID(model.nodeIPP, model.nodePort))
            }
        }
    }

    init {
        with(root) {
            left {
                vbox {
                    style{
                        spacing = 10.px
                        paddingTop = 10
                        paddingLeft = 10
                        paddingBottom = 10
                    }
                    listview(list){
                        cellFormat { text = it.toString() }

                        onUserSelect {
                            selected = it
                            renderChat()
                        }
                    }

                    button("Disconnect"){
                        hgrow = Priority.ALWAYS
                        maxWidth = Double.POSITIVE_INFINITY
                    }.enableWhen(uiEnable).setOnAction {
                        runBlocking {
                            node.disconnect()
                            (scene.window as Stage).close()
                        }
                    }
                }
            }

            right {
                form {
                    visibleWhen(enable)
                    fieldset {
                        style{
                            paddingBottom = 0
                        }
                        label(){
                            titleField = this
                            style {
                                fontSize = 15.px
                                fontWeight = FontWeight.BOLD
                            }
                        }
                        vgrow = Priority.ALWAYS
                        maxWidth = Double.POSITIVE_INFINITY

                        textarea() {
                            chatField = this
                            vgrow = Priority.ALWAYS
                            maxWidth = Double.POSITIVE_INFINITY
                        }.editableProperty().value = false

                        hbox {
                            style{
                                spacing = 10.px
                                paddingTop = 10
                            }
                            textfield {
                                messageField = this
                                hgrow = Priority.ALWAYS
                                maxWidth = Double.POSITIVE_INFINITY
                                promptText = "type in something"
                            }
                            button("Send").enableWhen(uiEnable).setOnAction {
                                uiEnable.set(false)

                                val text = messageField.text
                                GlobalScope.launch {
                                    node.cmd(Task.ActionRequest(Command.SendMessage(selected!!,  text)))
                                }
                                messageField.text = ""

                            }
                        }
                    }
                }
            }
        }
    }

    fun renderChat(){
        enable.set(selected != null)
        val chat = chats[selected]
        if (chat != null){
            titleField.text = chat.id.toString()
            chatField.text = chat.formatMessages()
        }

    }

}
