package distributed_chat.ui

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

class LoginScreen : View("Login") {
    val model: LoginViewModel by inject()

    override val root = form {
        label("Enter IP and port of any known node"){
            style {
                paddingBottom = 10
                fontWeight = FontWeight.BOLD
            }
        }

        fieldset(labelPosition = Orientation.VERTICAL) {
            field("IP společníka") {
                textfield(model.nodeIPProperty){

                }

            }
            field("Port společníka") {
                textfield(model.nodePortProperty){
                    filterInput { it.controlNewText.isInt() }
                    promptText = "1234"
                }
            }

            field("Váš port") {
                textfield(model.yourPortProperty){
                    filterInput { it.controlNewText.isInt() }
                    promptText = "1234"
                }.required()
            }
            button("Connect") {
                enableWhen(this@LoginScreen.model.valid)
                isDefaultButton = true
                useMaxWidth = true
                action {
                    runAsyncWithProgress {
                        runLater {
                            find(LoginScreen::class).replaceWith(ChatScreen::class, sizeToScene = true, centerOnScreen = true)

                        }
                    }
                }
            }
        }

    }
}

class LoginViewModel: ViewModel() {
    val nodePortProperty = SimpleIntegerProperty(this, "node_port", 350)
    var nodePort by nodePortProperty

    val nodeIPProperty = SimpleStringProperty(this, "node_ip", "127.0.0.1")
    var nodeIPP by nodeIPProperty

    val yourPortProperty = SimpleIntegerProperty(this, "your_port", 350)
    var yourPort by yourPortProperty
}