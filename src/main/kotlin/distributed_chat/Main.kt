
package distributed_chat

import distributed_chat.core.ClusterNode
import distributed_chat.core.Command
import distributed_chat.core.NodeID
import distributed_chat.core.Task
import distributed_chat.ui.ChatApp
import kotlinx.coroutines.*
import tornadofx.launch



fun main(args: Array<String>){
    launch<ChatApp>(args)
}





/*suspend fun test1() {
    val node1 = ClusterNode(NodeID("127.0.0.1", 3500))

    delay(1000)

    val node2 = ClusterNode(NodeID("127.0.0.1", 3501))
    node2.connect(NodeID("127.0.0.1", 3500))


    delay(1000)

    val node3 = ClusterNode(NodeID("127.0.0.1", 3502))
    node3.connect(NodeID("127.0.0.1", 3501))

    delay(1000)
    println(node1.nodes.keys)
    println(node2.nodes.keys)
    println(node3.nodes.keys)

    node1.clock.incrementClock(10)
    node2.clock.incrementClock(10)
    node3.clock.incrementClock(10)

    node1.cmd(Task.ActionRequest(Command.SendMessage(node3.id, "AHOJ")))
    node2.cmd(Task.ActionRequest(Command.SendMessage(node1.id, "AHOJ")))
    node3.cmd(Task.ActionRequest(Command.SendMessage(node2.id, "AHOJ")))
    delay(10000)
}*/
