package distributed_chat.core

import kotlin.math.max

data class LogicClock(var counter: Int){
    /**
     * @param eventTime -1 event doesnt have any causality
     */
    fun incrementClock(eventTime: Int = -1){
        counter = max(eventTime, counter) + 1
    }
}


/**
 * Kotlin representation of mathematical total ordering between nddes
 */
data class TotalOrdering(var counter: Int, val id: NodeID) : Comparable<TotalOrdering>{
    private val _ip = id.ip.replace(".", "").toLong() //transform ip to Integer

    override fun toString() = "$counter:${id.port}"

    /**
     * Custom compare by defined by: counter, port, IP to define total ordering
     */
    override fun compareTo(other: TotalOrdering) = when {
        counter == other.counter -> when {
            id.port == other.id.port -> when {
                _ip == other._ip  -> 0
                _ip < other._ip -> -1
                else -> 1
            }
            id.port < other.id.port -> -1
            else -> 1
        }
        counter < other.counter -> -1
        else -> 1
    }
    
}
