package distributed_chat.core

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.CONFLATED
import kotlinx.coroutines.launch
import mu.KotlinLogging

/**
 * Data class which carries data between View and business logic
 */
data class UIUpdate(val chats: HashMap<NodeID, Chat>, val uiEnable: Boolean)

/**
 * Set of commands for controlling [ClusterNode]
 */
sealed class Command{
    data class Connect(val id: NodeID): Command()
    object Disconnect : Command()
    class SendMessage(val id: NodeID, val msg: String): Command()
}

/**
 * Higher abstraction of [Command] and [Message]
 */
sealed class Task{
    class IncomingMessage(val msg: Message):Task()
    class ActionRequest(val cmd: Command):Task()
}


open class ClusterNode(
    val id: NodeID,
    private val uiChannel: Channel<UIUpdate> = Channel(CONFLATED)
) {
    internal val logger = KotlinLogging.logger ("${id.ip}:${id.port}")

    internal val nodes = hashMapOf<NodeID, Chat>()
    internal val clock = LogicClock(0)
    private val mutex = Mutex(this)

    private val taskChannel: Channel<Task> = Channel()
    private val inputHandler = IncomingMessageHandler(id.port, taskChannel)
    internal val output = Messenger(this)

    val job: Job

    init {
        job = GlobalScope.launch {
            for (task in taskChannel) {
                when(task){
                    is Task.IncomingMessage -> onMessageReceived(task.msg)
                    is Task.ActionRequest ->when(task.cmd){
                        is Command.Connect -> connect(task.cmd.id)
                        is Command.Disconnect ->disconnect()
                        is Command.SendMessage ->sendMessage(task.cmd.id, task.cmd.msg)
                    }
                }
            }
            logger.info { "Closing command channel" }
        }

        inputHandler.listen()

        logger.info { "Up and Running" }
    }


    /**
     * Properly disconnect from node cluster
     */
    suspend fun disconnect(){
        try {
            logger.info { "Disconnecting" }
            nodes.keys.forEach {
                output.send(Message.Disconnect(id, clock.counter), it)
            }

            terminate()
        }catch (e: Exception){

        }
    }

    suspend fun terminate(){
        logger.info { "Terminating" }
        inputHandler.terminate()
        taskChannel.close()
    }

    /**
     * Register this node in cluster by asking any node for his known nodes.
     */
    suspend fun connect(id: NodeID){
        output.send(Message.RegistrationRequest(this.id, clock.counter), id)
    }

    /**
     * Send its own nodes to the registering nodes
     */
    private suspend fun respondWithClusterNodes(msg: Message.RegistrationRequest){
        nodes[msg.node] = Chat(msg.node)
        output.send(Message.RegisterResponse(id, clock.counter, nodes.keys), msg.node)
        updateUI()
    }

    /**
     * Receive a reply with cluster node ids and broadcast information about your existence to the cluster
     */
    private suspend fun processRegistration(acceptance: Message.RegisterResponse){
        logger.info { "Node registered in: ${acceptance.nodes}" }
        if (nodes.isEmpty()){
            acceptance.nodes.forEach {
                nodes[it] = Chat(it)
            }
            nodes[acceptance.node] = Chat(acceptance.node)
            nodes.remove(id)
        }

        nodes.keys.filter { it != acceptance.node }.forEach {
            output.send(Message.BroadcastRegistration(id, clock.counter), it)
        }

        updateUI()
    }

    /**
     * Add new node to your set of nodes
     */
    private suspend fun processConnectionRequest(msg: Message.BroadcastRegistration){
        nodes[msg.node] = Chat(msg.node)
        updateUI()
    }


    /**
     * If for any reason you cant connect to a [ClusterNode] broadcast its death to the cluster
     */
    internal suspend fun broadcastDeath(dead: NodeID){
        logger.info { "Broadcasting dead: $dead" }

        nodes.keys.minus(dead).forEach {
            output.sendWithoutChecking(Message.NodeConnectionLost(id, clock.counter, dead), it)
        }

        if (nodes.contains(dead))
            nodeHashDisconnected(dead)
    }

    /**
     * Removes a node from you node lists, Notify [Mutex] to try enter critical section
     */
    private suspend fun nodeHashDisconnected(id: NodeID){
        nodes.remove(id)
        mutex.onNodeDisconnected(id)

        updateUI()
    }

    /**
     * Process incoming message from other node
     */
    private suspend fun onMessageReceived(msg: Message) {
        clock.incrementClock(msg.clock)
        when(msg){
            is Message.CSRequest -> mutex.onOuterCSRequest(msg)
            is Message.Chat -> processChatMessage(ChatMessage(msg.node, id, msg.clock, msg.msg))
            is Message.CRRequestReply -> mutex.onCSReply(msg)
            is Message.RegistrationRequest -> respondWithClusterNodes(msg)
            is Message.RegisterResponse -> processRegistration(msg)
            is Message.BroadcastRegistration -> processConnectionRequest(msg)
            is Message.Disconnect -> nodeHashDisconnected(msg.node)
            is Message.NodeConnectionLost -> nodeHashDisconnected(msg.death)
        }
    }

    /**
     * Add new message including your own messages to the chat history and update GUI

     */
    private suspend fun processChatMessage(msg: ChatMessage){
        //If this node is the sender put the message in receivers chat
        val chat = if (msg.from == id)
            nodes[msg.to]
        else
            nodes[msg.from]

        if (chat != null)
            chat.history.add(msg)
        else
            logger.error { "Received message from unknown Node: ${msg.from}"}

        updateUI(true)
    }

    /**
     * Requests a critical section lock among other nodes, sends a message and release the lock
     */
    private fun sendMessage(id: NodeID, msg: String){
        check( this@ClusterNode.id != id) {"Cant send message to yourself"}

        mutex.request {
            this@ClusterNode.id

            val message = Message.Chat(this@ClusterNode.id, clock.counter, msg)
            output.send(message, id)

            processChatMessage(ChatMessage(this@ClusterNode.id, id, clock.counter, msg ))
        }

    }

    /**
     * Interface to asynchronous communication with this node
     */
    suspend fun cmd(task: Task){
        taskChannel.send(task)
    }

    /**
     * Notify UI channel about state changes
     */
    private suspend fun updateUI(enable: Boolean = !mutex.isRequesting()) {
        uiChannel.send(UIUpdate(nodes, enable))
    }

}