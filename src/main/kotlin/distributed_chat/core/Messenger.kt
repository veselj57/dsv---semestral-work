package distributed_chat.core

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.io.IOException
import java.net.Socket
import java.nio.ByteBuffer

/**
 * Interface for communication with other nodes
 */
class Messenger(val node: ClusterNode){
    private val logger = KotlinLogging.logger ("x:${node.id.port}")

    /**
     * Sends a message to another node
     * Message structure:
     *  4 bytes of message length
     *  n bytes of actual message
     */
    fun sendWithoutChecking(message: Message, id: NodeID) = runBlocking(Dispatchers.IO){
        node.clock.incrementClock()
        logger.info { "\t t=${node.clock.counter}, to=$id Sending: $message" }

        val data = message.serialize()
        val length = ByteBuffer.allocate(4).putInt(data.size).array()

        val raw = ByteArray(data.size + 4)
        length.copyInto(raw, 0)
        data.copyInto(raw, 4)

        val socket = Socket(id.ip, id.port)
        val output = socket.getOutputStream()

        output.write(raw)
        output.flush()

        delay(100)

        socket.close()
    }

    /**
     * Send interface with delivery check
     * if node is lost, notify other nodes about its death
     */
    suspend fun send(message: Message, id: NodeID): Boolean{
        return try {
            sendWithoutChecking(message, id)
            true
        }catch (e: IOException){
            e.printStackTrace()
            delay(1000)


            node.broadcastDeath(id)
            false
        }
    }
}