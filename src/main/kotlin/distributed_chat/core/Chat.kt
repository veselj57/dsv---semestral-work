package distributed_chat.core

import kotlinx.serialization.Serializable

@Serializable
data class ChatMessage(var from: NodeID, var to: NodeID, val clock: Int, val msg: String){
    override fun toString() ="$from/$clock    $msg"
}


class Chat(val id: NodeID, val history: MutableList<ChatMessage> = mutableListOf()){
    fun formatMessages() =  history.fold(""){ acc, msg -> acc + msg + "\n"   }
}