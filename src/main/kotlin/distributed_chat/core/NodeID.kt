package distributed_chat.core

import kotlinx.serialization.Serializable

@Serializable
data class NodeID(val ip:String, val port:Int){
    override fun toString() = "$ip:$port"
}