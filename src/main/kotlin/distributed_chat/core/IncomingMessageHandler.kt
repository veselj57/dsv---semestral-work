package distributed_chat.core

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException
import java.nio.ByteBuffer

class IncomingMessageHandler(val port: Int, val channel: Channel<Task>) {
    var server: ServerSocket? = null
    var job: Job? = null

    /**
     * Open input connection to this [ClusterNode]
     * Creates Listening coroutine which on oncoming request create message handler coroutine
     */
    fun listen(){
        server = ServerSocket(port)
        job = GlobalScope.launch(Dispatchers.IO+ CoroutineName("Message accepting coroutine")) {
            while (isActive){
                val connection = try {
                      server!!.accept()
                }catch (e: SocketException){
                    cancel()
                    break
                }

                launch{
                    processMessage(connection)
                }
            }
        }
    }

    /**
     * close input connection to this [ClusterNode]
     */
    fun terminate(){
        job?.cancel()
        server?.close()
    }

    /**
     * Parse Node Communication Protocol with [Message]
     * 4 bytes of message length
     * n bytes of actual ProtoBuf representation of [Message]
     */
    fun processMessage(socket: Socket) = runBlocking(Dispatchers.IO) {
        val input = socket.getInputStream()

        val length = ByteArray(4) { 0 }
        input.read(length)

        val raw = ByteArray(ByteBuffer.wrap(length).int) { 0 }
        input.read(raw)

        channel.send(Task.IncomingMessage(Message.deserialize(raw)))
    }

}