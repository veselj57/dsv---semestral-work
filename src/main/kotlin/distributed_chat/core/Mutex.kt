package distributed_chat.core

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import javax.xml.bind.JAXBElement

typealias CriticalSection = suspend Mutex.()->Unit

/**
 * Cluster Mutex implemented by Ricart-Agrawala Mutual Exclusion Algorithm
 */
class Mutex(val node: ClusterNode){

    /**
     * Indication whether this node got reply from other nodes
     */
    enum class CSReplay{REQUIRED, GRANTED}

    /**
     * Lower priority critical section requests
     */
    private val deferredCSRequest = mutableSetOf<NodeID>()

    /**
     * Set of nodes this node should ask for a permission to be granted access to the CS
     */
    private val replays = mutableMapOf<NodeID, CSReplay>()

    /**
     * Critical Section call back
     */
    private var cs: CriticalSection? = null

    private var myRequest = TotalOrdering(0, node.id)

    /**
     * Request critical section
     * WARNING: second request cant be made before first released itself
     */
    @Synchronized
    fun request(critical_section: CriticalSection): Boolean{
        node.logger.info { "Requesting CS: clock: ${node.clock.counter}" }
        if (cs != null){
            return false
        }

        cs = critical_section
        myRequest = TotalOrdering(node.clock.counter, node.id)
        node.nodes.keys.forEach { replays[it] = CSReplay.REQUIRED }

        GlobalScope.async(Dispatchers.IO) {
            replays.forEach{ node.output.send(Message.CSRequest(node.id, myRequest.counter), it.key)}
        }

        return true
    }

    /**
     * Whenever a reply is received try acess the critical section
     */
    suspend fun onCSReply(msg: Message.CRRequestReply){
        replays[msg.node] = CSReplay.GRANTED
        tryLock()
    }

    /**
     * Handler for incoming critical section requests
     */
    suspend fun onOuterCSRequest(msg: Message.CSRequest){
        val other = TotalOrdering(msg.clock, msg.node)

        if(cs != null &&  myRequest < other){
            node.logger.info { "\tDeferred: cs = ${cs!=null}, my=$myRequest other=$other" }
            deferredCSRequest.add(msg.node)
        }else{
            node.output.send(Message.CRRequestReply(node.id, node.clock.counter), msg.node)
        }
    }

    /**
     * When a node is disconnected from the cluster, the permission for critical section is not needed
     */
    suspend fun onNodeDisconnected(id: NodeID){
        replays.remove(id)
        tryLock()
    }

    /**
     * Check if all conditions for access to the critical section are met
     */
    private suspend fun tryLock(){
        if(replays.values.filter { it == CSReplay.REQUIRED }.count() == 0 && cs != null){
            cs!!.invoke(this)
            cs = null
            replays.clear()

            deferredCSRequest.forEach { node.output.send(Message.CRRequestReply(node.id, node.clock.counter), it) }
            deferredCSRequest.clear()
        }
    }

    fun isRequesting() = cs != null
}