package distributed_chat.core


import kotlinx.serialization.PolymorphicSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.protobuf.ProtoBuf


/**
 * Set Messages which can be send between nodes
 * Messages are primly synchronized to binary format Protobuf
 */
@Serializable
sealed class Message{
    abstract val clock: Int
    abstract val node: NodeID

    companion object{
        fun deserialize(data: ByteArray) = ProtoBuf(context).load(PolymorphicSerializer(Message::class), data) as Message
    }

    fun serialize() = ProtoBuf(context).dump(PolymorphicSerializer(Message::class), this)

    /* Lamport - Agrewala algorithm for Critical Section*/
    @Serializable
    data class CSRequest(override val node: NodeID, override val clock: Int): Message()
    @Serializable
    data class CRRequestReply(override val node: NodeID, override val clock: Int): Message()

    /* New node registration in cluster */
    @Serializable
    data class RegistrationRequest(override val node: NodeID, override val clock: Int) : Message()
    @Serializable
    data class RegisterResponse(override val node: NodeID, override val clock: Int, val nodes: Set<NodeID> = emptySet()) : Message()
    @Serializable
    data class BroadcastRegistration(override val node: NodeID, override val clock: Int) : Message()

    /* Business logic */
    @Serializable
    data class Chat(override val node: NodeID, override val clock: Int, val msg: String): Message()

    /* Connection lost Messages*/
    @Serializable
    data class Disconnect(override val node: NodeID, override val clock: Int): Message()
    @Serializable
    data class NodeConnectionLost(override val node: NodeID, override val clock: Int, val death: NodeID): Message()
}

/**
 * Wordy serializer for [Message]
 */
val context = SerializersModule {
    polymorphic(Message::class) {
        Message.CSRequest::class with Message.CSRequest.serializer()
        Message.Chat::class with Message.Chat.serializer()
        Message.CRRequestReply::class with Message.CRRequestReply.serializer()
        Message.RegistrationRequest::class with Message.RegistrationRequest.serializer()
        Message.BroadcastRegistration::class with Message.BroadcastRegistration.serializer()
        Message.RegisterResponse::class with Message.RegisterResponse.serializer()
        Message.Disconnect::class with Message.Disconnect.serializer()
        Message.NodeConnectionLost::class with Message.NodeConnectionLost.serializer()
    }
}