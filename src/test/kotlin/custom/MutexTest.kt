package custom

import distributed_chat.core.ClusterNode
import distributed_chat.core.Command
import distributed_chat.core.NodeID
import distributed_chat.core.Task
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue


class MutexTest {

    lateinit var node1:ClusterNode
    lateinit var node2:ClusterNode
    lateinit var node3:ClusterNode

    @BeforeEach
    fun before(): Unit = runBlocking{
        node1 = ClusterNode(NodeID("127.0.0.1", 3500))

        delay(1000)

        node2 = ClusterNode(NodeID("127.0.0.1", 3501))
        node2.connect(node1.id)

        delay(1000)

        node3 = ClusterNode(NodeID("127.0.0.1", 3502))
        node3.connect(node2.id)
        delay(1000)

        println(node1.nodes.keys)
        println(node2.nodes.keys)
        println(node3.nodes.keys)


    }

    @AfterEach
    fun clean(): Unit = runBlocking{
        println("----------start closing--------------")
        node1.terminate()
        node2.terminate()
        node3.terminate()
        delay(1000)
        println("----------closing end--------------")
    }


    @Test
    fun node_disconnect(): Unit = runBlocking {
        assertTrue(node2.nodes.containsKey(node1.id))
        assertTrue(node3.nodes.containsKey(node1.id))
        node1.disconnect()
        delay(1000)

        assertTrue(!node2.nodes.containsKey(node1.id))
        assertTrue(!node3.nodes.containsKey(node1.id))
    }

    @Test
    fun node_terminate(): Unit = runBlocking {
        node1.terminate()
        delay(1000)

        assertTrue(node2.nodes.containsKey(node1.id))
        assertTrue(node3.nodes.containsKey(node1.id))

        node2.cmd(Task.ActionRequest(Command.SendMessage(node1.id, "TEST")))

        delay(4000)

        assertTrue(!node2.nodes.containsKey(node1.id))
        assertTrue(!node3.nodes.containsKey(node1.id))
    }

    @Test
    fun test_deadlock(): Unit = runBlocking {


        node1.clock.incrementClock(10)
        node2.clock.incrementClock(10)
        node3.clock.incrementClock(10)

        node1.cmd(Task.ActionRequest(Command.SendMessage(node3.id, "NODE1_MESSAGE")))
        node2.cmd(Task.ActionRequest(Command.SendMessage(node1.id, "NODE2_MESSAGE")))
        node3.cmd(Task.ActionRequest(Command.SendMessage(node2.id, "NODE3_MESSAGE")))

        delay(1000)
        assertEquals(node3.nodes[node1.id]!!.history[0].msg, "NODE1_MESSAGE")
        assertEquals(node1.nodes[node2.id]!!.history[0].msg, "NODE2_MESSAGE")
        assertEquals(node2.nodes[node3.id]!!.history[0].msg, "NODE3_MESSAGE")
    }
}

