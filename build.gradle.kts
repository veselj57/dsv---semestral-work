import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


repositories {
    jcenter()
    maven("https://kotlin.bintray.com/kotlinx")
    mavenCentral()
}

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.50"
    id ("org.jetbrains.kotlin.plugin.serialization") version "1.3.50"
    application
}
application{
    mainClassName = "distributed_chat.MainKt"
}

group = "xxx.xxx"
version = "1"


dependencies {

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("org.jetbrains.kotlin:kotlin-reflect:1.3.50")

    implementation("org.jetbrains.kotlin:kotlin-serialization:1.3.50")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.13.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2")
    implementation("no.tornado:tornadofx:1.7.17")
    implementation("io.github.microutils:kotlin-logging:1.7.6")
    implementation("org.slf4j:slf4j-simple:1.7.26")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.4.2")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:5.4.2")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}


tasks.register<Jar>("fatJar") {
    archiveClassifier.set("fat")

    manifest{
        attributes["Main-Class"] = application.mainClassName
    }

    from(sourceSets.main.get().output)

    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })
}